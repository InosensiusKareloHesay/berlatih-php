<?php
function ubah_huruf($string){
    //kode di sini
    $arrayHuruf = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',];

    $kataBaru = "";
    for ($i = 0; $i < strlen($string); $i++) {
        $cari = array_search(substr($string, $i, 1), $arrayHuruf);
        $kataBaru = $kataBaru.$arrayHuruf[$cari+1];
    }
    return $kataBaru;
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu
?>